from diagrams import Cluster, Diagram, Node, Edge
from diagrams.custom import Custom
from diagrams.onprem.network import Traefik
from diagrams.onprem.auth import Oauth2Proxy
from urllib.request import urlretrieve

graph_attr = {
    "splines": "spline",
}

with Diagram("Forward Auth", show=False, direction="TB", graph_attr=graph_attr, outformat="png"):

    metallb = Custom("", "../resources/metal-lb.png")

    with Cluster(""):
        ingress = Traefik("ingress")
        oauth2proxy = Oauth2Proxy("forward auth")
        keycloak = Custom("", "../resources/keycloak.png")

        ingress >> Edge(label="http://domain/request/")  >> oauth2proxy >> keycloak
        oauth2proxy >> Edge(label="if KO, return error") >> ingress

    metallb >> ingress
